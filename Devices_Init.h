
IndividualDevice _deviceControls[AVAILABLE_RELAY_DEVICES] = {Socket_TV_NAME_INDEX,Socket_Amplifier_NAME_INDEX,Socket_Internet_NAME_INDEX,Socket_Fourth_NAME_INDEX};

void InitializeRelayControlledDevices()
{
  byte hr = 0, min = 0;
  _deviceControls[Socket_TV_INDEX].SetTotalNbPrograms(1);
  _deviceControls[Socket_TV_INDEX].SetAssociatedDigitalPinOrSlaveDeviceIndex(Socket_TV_Pin );
  _deviceControls[Socket_TV_INDEX].SetIfOnMaster(true);
  //InitializeDeviceProgramOptions(_deviceControls[Socket_TV_INDEX].GetProgram(0),false/*always on not possible*/,true/*time controlled possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,false /* upto time not possible*/);
  //_deviceControls[Socket_TV_INDEX].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  //_deviceControls[Socket_TV_INDEX].GetProgram(0)->SetProgramDaySettingType(MTWTFSS);
 // byte hr = 10, min = 0;
 // _deviceControls[Socket_TV_INDEX].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
 // hr = 22; min = 0;
 // _deviceControls[Socket_TV_INDEX].GetProgram(0)->SetEndHrAndMin(hr /* end hr*/,min/*end min*/);
  InitializeDeviceProgramOptions(_deviceControls[Socket_TV_INDEX].GetProgram(0),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Socket_TV_INDEX].GetProgram(0)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);
  
  _deviceControls[Socket_Amplifier_INDEX].SetTotalNbPrograms(1);
  _deviceControls[Socket_Amplifier_INDEX].SetAssociatedDigitalPinOrSlaveDeviceIndex(Socket_Amplifier_Pin );
  _deviceControls[Socket_Amplifier_INDEX].SetIfOnMaster(true);
  InitializeDeviceProgramOptions(_deviceControls[Socket_Amplifier_INDEX].GetProgram(0),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Socket_Amplifier_INDEX].GetProgram(0)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);

  _deviceControls[Socket_Internet_INDEX].SetTotalNbPrograms(2);
  _deviceControls[Socket_Internet_INDEX].SetAssociatedDigitalPinOrSlaveDeviceIndex(Socket_Internet_Pin );
  _deviceControls[Socket_Internet_INDEX].SetIfOnMaster(true);
  InitializeDeviceProgramOptions(_deviceControls[Socket_Internet_INDEX].GetProgram(0),false/*always on not possible*/,true/*time controlled possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,false /* upto time not possible*/);
  _deviceControls[Socket_Internet_INDEX].GetProgram(0)->SetProgramOnOffControlType(Circuit_TimeControlled);
  _deviceControls[Socket_Internet_INDEX].GetProgram(0)->SetProgramDaySettingType(MTWTFSS);
  hr = 8; min = 0;
  _deviceControls[Socket_Internet_INDEX].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 23; min = 0;
  _deviceControls[Socket_Internet_INDEX].GetProgram(0)->SetEndHrAndMin(hr/* end hr*/,min/*end min*/);
  InitializeDeviceProgramOptions(_deviceControls[Socket_Internet_INDEX].GetProgram(1),false/*always on not possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,true /* upto time possible*/);
  _deviceControls[Socket_Internet_INDEX].GetProgram(1)->SetProgramOnOffControlType(Circuit_ValidForUptoTime);


  _deviceControls[Socket_External_Socket_INDEX].SetTotalNbPrograms(1);
  _deviceControls[Socket_External_Socket_INDEX].SetAssociatedDigitalPinOrSlaveDeviceIndex(Socket_ExternalSocket_Pin );
  _deviceControls[Socket_External_Socket_INDEX].SetIfOnMaster(true);
  InitializeDeviceProgramOptions(_deviceControls[Socket_External_Socket_INDEX].GetProgram(0),true/*always on possible*/,false/*time controlled not possible*/,false/*sensor controlled not possible*/,false/*any light on not possible*/,false /* upto time not possible*/);
  _deviceControls[Socket_External_Socket_INDEX].GetProgram(0)->SetProgramOnOffControlType(Circuit_OFF);
  hr = 0, min = 0;
  _deviceControls[Socket_External_Socket_INDEX].GetProgram(0)->SetStartHrAndMin(hr /* start hr*/,min/*start min*/);
  hr = 24; min = 0;
}

void DeviceOnOff(byte iIndexOfDevice, bool iOn)
{
    byte pin = _deviceControls[iIndexOfDevice].GetAssociatedDigitalPinOrSlaveDeviceIndex();
    pinMode(pin,OUTPUT);
    if(iOn)
       digitalWrite(pin,RELAY_ON);
    else
       digitalWrite(pin,RELAY_OFF);
}
