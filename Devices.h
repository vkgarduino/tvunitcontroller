
/*program types
 * 0: monday, tuesday, wednesday, thursday, friday, saturaday, sunday
 * 1: monday, wendesday, friday
 * 2: tuesday, thursday, saturday
 */

const byte MaxValueOfProgramType = 4;
enum Program_DAY_SETTING_Type {MTWTFSS = 0, MWF = 1, TuThSa = 2, MTWTF =3, SaSu =  MaxValueOfProgramType};
enum Program_ON_OFF_CONTROL_Type{Circuit_OFF = 0,Circuit_ON = 1,Circuit_TimeControlled = 2,Circuit_SensorControlled = 3,Circuit_AnyLightOnControlled = 4,Circuit_ValidForUptoTime = 5};

bool GetIsProgramEnabledBasedOnCurrentDay(Program_DAY_SETTING_Type programType)
{
  DateTime& currTime = GetRtcTime();
  uint8_t day = currTime.dayOfWeek();
  switch(programType)
  {
    case MWF: 
       if(day == TUESDAY || day == THURSDAY || day == SATURDAY || day == SUNDAY)
          return false;
       break;

    case TuThSa: 
       if(day == MONDAY || day == WEDNESDAY || day == FRIDAY || day == SUNDAY)
          return false;
       break;

    case MTWTF: 
       if(day == SATURDAY|| day == SUNDAY)
          return false;
       break;    
    
    case SaSu: 
       if(day == MONDAY || day == TUESDAY || day == WEDNESDAY || day == THURSDAY || day == FRIDAY)
          return false;
       break;    

    default:
         break;
    }
  return true;
}

class DeviceProgramSettings
{
  byte data;  //bit 0,1,2,3 for ON, OFF, AUTO
                //bit 4,5,6,7 define program type and handled by GetIsProgramEnabledBasedOnCurrentDay and SetProgramDaySettingType fn
  byte data2;   //0 if time controlled possible, 1 if sensor controlled pssible, 2 if any light on, 3 if on possible, 4 if Circuit_ValidForUptoTime possible or not
  byte start_h, start_m; 
  byte end_h, end_m; 
  int _year;

        /*
       * Circuit_TimeControlled:
       *    _start_h : start hour
       *    _start_m : end time
       *    _end_h_Or_Duration : end hour
       *    _end_m   : end min
       *    
       */

        /*
       * Circuit_TimeControlled and device is duration based (duration in second)
       *    _start_h : start hour
       *    _start_m : end time
       *    _end_h_Or_Duration : seconds if time is duration based
       *    _end_m   : not used
       *    
       */

       
       /*
       * Circuit_ValidForUptoTime:
       *    _start_h : date
       *    _start_m : month
       *    _year     : year
       *    _end_h_Or_Duration : hour
       *    _end_m   : min
       *    
       */
                
public:

  public:
  DeviceProgramSettings()
  {
     start_h =1; start_m = 1; end_h = 0; end_m = 0;_year = 2001;
     SetIfTimeControlledPossible(false);
     SetIfSensorControlledPossible(false);
     SetIfAnyLightOnControllPossible(false);
     SetIfUptoTimePossible(false);
     SetIfOnPossible(false);
     SetProgramOnOffControlType(Circuit_OFF);
     SetProgramDaySettingType(MTWTFSS);
  }

  void SaveInfo()
  {
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(data);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(start_h);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(start_m);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(end_h);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(end_m);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(_year/100);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress(_year%100);

    /*
     Serial.println (data);
       Serial.println (data2);
       Serial.println (start_h);
       Serial.println (start_m);
       Serial.println (end_h);
       Serial.println (end_m);
       */

  }

  void ReadInfo( )
  {
    ReadSerializedValue(data);
    ReadSerializedValue(start_h);
    ReadSerializedValue(start_m);
    ReadSerializedValue(end_h);
    ReadSerializedValue(end_m);

    byte val1 = 0;
    byte val2 = 0;
    ReadSerializedValue(val1);
    ReadSerializedValue(val2);
    _year = val1*100+val2;

    /*
     Serial.println (data);
       Serial.println (data2);
       Serial.println (start_h);
       Serial.println (start_m);
       Serial.println (end_h);
       Serial.println (end_m);
       */
  }


  void SetStartHrAndMin(byte & hr, byte &min ){start_h = hr;start_m = min;}
  void GetStartHrAndMin(byte & hr, byte &min ){hr = start_h;min = start_m;}
  void SetEndHrAndMin(byte & hr, byte &min ){end_h = hr;end_m = min;}
  void GetEndHrAndMin(byte & hr, byte &min ){hr = end_h;min = end_m;}
  void SetDateMonthYear(byte & date, byte &month, int &year){start_h = date;start_m = month; _year = year;}
  void GetDateMonthYear(byte & date, byte &month, int &year){date = start_h;month = start_m; year = _year;}

  
  void SetDurationInSeconds(byte & duration){ if(duration > 99) return; end_h = duration;}
  void GetDurationInSeconds(byte & duration){duration = end_h;}
  void SetYear(byte & year){ _year = year;}
  void GetYear(byte & year){year = _year;}
  

  void SetProgramOnOffControlType( Program_ON_OFF_CONTROL_Type value)             {SetBitsInTarget(&data,value,0,3);}
  Program_ON_OFF_CONTROL_Type GetProgramOnOffControlType()                        {return (Program_ON_OFF_CONTROL_Type) GetValueFromBits(&data,0,3);}
  void SetProgramDaySettingType(Program_DAY_SETTING_Type value)         {SetBitsInTarget(&data,value,4,7);}
  Program_DAY_SETTING_Type GetProgramDaySettingType()                   {return (Program_DAY_SETTING_Type)GetValueFromBits(&data,4,7);}
  
  void SetIfTimeControlledPossible(bool iVal)      {return SetBoolValueInBit(&data2,iVal,0);}
  bool GtIfTimeControlledPossible()                {return GetBoolValueFromBit(&data2,0);}
  void SetIfSensorControlledPossible(bool iVal)    {return SetBoolValueInBit(&data2,iVal,1);}
  bool GetIfSensorControlledPossible()             {return GetBoolValueFromBit(&data2,1);}
  void SetIfAnyLightOnControllPossible(bool iVal)  {return SetBoolValueInBit(&data2,iVal,2);}
  bool GetIfAnyLightOnControllPossible()           {return GetBoolValueFromBit(&data2,2);}
  void SetIfOnPossible(bool iVal)                  {return SetBoolValueInBit(&data2,iVal,3);}
  bool GetIfOnPossible()                           {return GetBoolValueFromBit(&data2,3);}
  void SetIfUptoTimePossible(bool iVal)            {return SetBoolValueInBit(&data2,iVal,4);}
  bool GetIfUptoTimePossible()                     {return GetBoolValueFromBit(&data2,4);}
};

class IndividualDevice
{
  byte data;//bit 0,1,2,3,4  for associated digital pin (slave device index if device is on slave), 5,6, for nb progrmas, 7 for end time as duration (seconds < 99)
  DeviceProgramSettings * program;
  byte data2;//bit 0,1,2,3,4,5,6 for name inddex 7 if on master or slave
public:

  IndividualDevice(byte iNameIndex)
  {
      data =0;
      data2 = 0;
      program = 0;
      SetIfTimeInDuration(false);
      SetIfOnMaster(true);
      SetTotalNbPrograms(0);
      SetNameIndex(iNameIndex);
  }

  DeviceProgramSettings * GetProgram(byte iIndex) {if(iIndex >= GetTotalNbPrograms())  return 0; return &program[iIndex];}

  void SaveInfo()
  {

    for(byte i = 0; i< GetTotalNbPrograms(); i++)
    {
       program[i].SaveInfo();
    }  
 }

  void ReadInfo()
  {
    for(byte i = 0; i< GetTotalNbPrograms(); i++)
    {
       program[i].ReadInfo();
    }
 }
  
  void SetNameIndex(byte iValue)                                { SetBitsInTarget(&data2,iValue,0,6);}
  byte GetNameIndex()                                { return GetValueFromBits(&data2,0,6);}
  byte GetAssociatedDigitalPinOrSlaveDeviceIndex()                  {return GetValueFromBits(&data,0,4);} 
  void SetAssociatedDigitalPinOrSlaveDeviceIndex(byte iValue)       {SetBitsInTarget(&data,iValue,0,4);}     
  byte GetTotalNbPrograms()                       {return GetValueFromBits(&data,5,6);}
  void SetTotalNbPrograms(byte iValue)            {
                                                   SetBitsInTarget(&data,iValue, 5,6);  
                                                   if(iValue > 0) 
                                                      program = new DeviceProgramSettings[iValue];
                                                  }
  bool GetIfTimeInDurationOrNbOfRound()                      {return GetBoolValueFromBit(&data,7);}
  void SetIfTimeInDuration(bool iValue)           {SetBoolValueInBit(&data,iValue,7);}

  bool GetIfOnMaster()                      {return GetBoolValueFromBit(&data2,7);}
  void SetIfOnMaster(bool iValue)           {SetBoolValueInBit(&data2,iValue,7);}

  bool IsDeviceOnThisMin_BasedOnTimerAndOnStatus(byte *oSecondsDeviceOnIfDurationBased=0);
  bool IsDeviceOnThisMin_BasedOnUptoTime();
  bool HasAnyLightControlledTypeProgram();
};


bool IndividualDevice::IsDeviceOnThisMin_BasedOnTimerAndOnStatus(byte *oSecondsDeviceOnIfDurationBased)
{
    for(byte i = 0; i< GetTotalNbPrograms(); i++)
    {
       DateTime& currTime = GetRtcTime();

      
       if(Circuit_ON == program[i].GetProgramOnOffControlType())
          return true;
       else if(Circuit_OFF == program[i].GetProgramOnOffControlType())
          continue;
       else if(program[i].GetProgramOnOffControlType() != Circuit_TimeControlled)   
       {
       	  continue;
       }
       if(!GetIsProgramEnabledBasedOnCurrentDay(program[i].GetProgramDaySettingType()))
          continue;
 

       byte start_h, start_m; 
       byte end_h, end_m; 

       program[i].GetStartHrAndMin(start_h,start_m);
       
       if(!GetIfTimeInDurationOrNbOfRound())
       {
           if(currTime.hour() < start_h)
              continue;
           if( (currTime.hour() == start_h) && (currTime.minute() < start_m))
              continue;
           program[i].GetEndHrAndMin(end_h,end_m);
           if(currTime.hour() > end_h)
               continue;
           else if((currTime.hour() == end_h) && (currTime.minute() > end_m))
             continue;
           return true;
       }
       else
       {
          if( (currTime.hour() != start_h) || (currTime.minute() != start_m))
            continue;

         if(GetIfTimeInDurationOrNbOfRound() && oSecondsDeviceOnIfDurationBased)
           program[i].GetDurationInSeconds(*oSecondsDeviceOnIfDurationBased);
         return true;
       }
    }
    return false;
}


bool IndividualDevice::IsDeviceOnThisMin_BasedOnUptoTime()
{
    for(byte i = 0; i< GetTotalNbPrograms(); i++)
    {
       DateTime& currTime = GetRtcTime();
       if(Circuit_ValidForUptoTime == program[i].GetProgramOnOffControlType())
       {
           byte date, month;
           int year; 
           byte end_h, end_m; 
           program[i].GetDateMonthYear(date, month, year);
           program[i].GetEndHrAndMin(end_h,end_m);

           if( currTime.year() > year || currTime.month() > month || currTime.date() > date)
              return false;
           if( year > currTime.year())
              return true;
           if( year == currTime.year() && month > currTime.month())
              return true;
           if( year == currTime.year() && month == currTime.month() && date > currTime.date())
              return true;
       
           if(currTime.hour() > end_h)
              return false;
           if( (currTime.hour() == end_h) && (currTime.minute() > end_m))
              return false;
           return true;
       }
    }
    return false;
}


bool IndividualDevice::HasAnyLightControlledTypeProgram()
{
    for(byte i = 0; i< GetTotalNbPrograms(); i++)
    {
       if(program[i].GetProgramOnOffControlType() == Circuit_AnyLightOnControlled )   
          return true;
    }
    return false;
}

void InitCurrentDeviceStatus()
{   
    for(byte i = 0; i<AVAILABLE_RELAY_DEVICES; i++)
      currentDeviceStatus[i] = false;
}

void InitializeDeviceProgramOptions(DeviceProgramSettings *iDeviceProgram, bool IfOnPossible,bool IfTimeControlledPossible, bool iSensorControlledPossible, bool ifAnyLightOnPossible, bool ifUptoTimePossible)
{
   iDeviceProgram->SetIfOnPossible(IfOnPossible);
   iDeviceProgram->SetIfTimeControlledPossible(IfTimeControlledPossible);
   iDeviceProgram->SetIfSensorControlledPossible(iSensorControlledPossible);
   iDeviceProgram->SetIfAnyLightOnControllPossible(ifAnyLightOnPossible);
   iDeviceProgram->SetIfUptoTimePossible(ifUptoTimePossible);
}
