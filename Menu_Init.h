void InitializeSettingMenus()
{
  MainMenu * mainMenu = new MainMenu(3,0);

  
  const byte _nbSpecialMenus = 4;
  static SpecialMenuEachItemInfo specialMenuItems[_nbSpecialMenus];
  
  specialMenuItems[0].SetInfo(uptoTimeOnlyNetTextName, 1, false);
  specialMenuItems[0].AppendDeviceIndex(Socket_Internet_INDEX);

  specialMenuItems[1].SetInfo(uptoTimeOnlyTvTextName, 1, false);
  specialMenuItems[1].AppendDeviceIndex(Socket_TV_INDEX);

  specialMenuItems[2].SetInfo(uptoTimeOnlyAmpTextName, 1, false);
  specialMenuItems[2].AppendDeviceIndex(Socket_Amplifier_INDEX);
/*
  specialMenuItems[3].SetInfo(uptoTimeTvAmpAndNetTextName, 3, false);
  specialMenuItems[3].AppendDeviceIndex(Socket_Internet_INDEX);
  specialMenuItems[3].AppendDeviceIndex(Socket_TV_INDEX);
  specialMenuItems[3].AppendDeviceIndex(Socket_Amplifier_INDEX);
*/  
  specialMenuItems[3].SetInfo(SocketOnOffTextName, 1, true);
  specialMenuItems[3].AppendDeviceIndex(Socket_External_Socket_INDEX);
  
  _SpecialMenu = new Menu_SpecialMenu(mainMenu,uptoTimeTextName,_nbSpecialMenus,specialMenuItems);
  mainMenu->AddItem(_SpecialMenu);
  mainMenu->AddItem(new RelayControlledDeviceMenu(mainMenu,devicesTextName));
  mainMenu->AddItem(new DateAndTimeMenu(mainMenu,dateTimeTextName));
  _currentlyActiveMenu = _SpecialMenu;
}
