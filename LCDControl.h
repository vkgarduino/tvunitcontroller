/// *************************************************************** LCD RELATED ********************************************
#ifdef AQUARIUM_CONTROL
void sendTxtSerially(const char * iVal)
{
  Serial.print(iVal);
}

void StartSerialCommand()
{
  sendTxtSerially(MASTER_COMMAND_PREFIX);
}
void SendSerialCmdContent_Text(const char * iVal)
{
  sendTxtSerially(iVal);
}
void SendSerialCmdContent_Byte(const byte iVal)
{
  Serial.print(iVal);
}

void SetSerialCommandRelimiter()
{
  sendTxtSerially(MASTER_COMMAND_RELIMITER);
}
void FinishSerialCommand()
{
  sendTxtSerially(MASTER_COMMAND_POSTFIX);
}
#else
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
#endif

void SetLcdCursor(byte column, byte row)
{
  #ifdef AQUARIUM_CONTROL
  StartSerialCommand();
  SendSerialCmdContent_Text(LCD_SET_CURSOR_COMMAND_NAME);
  SetSerialCommandRelimiter();
  SendSerialCmdContent_Byte(column);
  SetSerialCommandRelimiter();
  SendSerialCmdContent_Byte(row);
  FinishSerialCommand();
 #else
   lcd.setCursor(column,row);
 #endif
}

void SetLcdBlink(bool iVal)
{
  #ifdef AQUARIUM_CONTROL
  StartSerialCommand();
  SendSerialCmdContent_Text(LCD_BLINK_COMMAND_NAME);
  SetSerialCommandRelimiter();
  if(iVal)
     SendSerialCmdContent_Text(CommandOn);
 else
     SendSerialCmdContent_Text(CommandOff);
 FinishSerialCommand();
 #else
	if(iVal)
      lcd.blink();
    else
      lcd.noBlink();
 #endif
}

void InitLcdRow(byte row)//0 for row 1, 1 for row 2, 10 for both rows
{
  #ifdef AQUARIUM_CONTROL
  StartSerialCommand();
  SendSerialCmdContent_Text(LCD_INIT_ROW_COMMAND_NAME);
  SetSerialCommandRelimiter();
  SendSerialCmdContent_Byte(row);
  FinishSerialCommand();
  #else
   lcd.setCursor(0,row);
   lcd.print("                ");
   SetLcdCursor(0,row);
  #endif
}

void AppendTextInLcd(const char * iString)
{
  #ifdef AQUARIUM_CONTROL
  StartSerialCommand();
  SendSerialCmdContent_Text(LCD_APPEND_TEXT_COMMAND_NAME);
  SetSerialCommandRelimiter();
  SendSerialCmdContent_Text(iString);
  FinishSerialCommand();
  #else
   lcd.print(iString);
  #endif
}

void AppendByte(byte iVal)
{
  #ifdef AQUARIUM_CONTROL
  StartSerialCommand();
  SendSerialCmdContent_Text(LCD_APPEND_TEXT_COMMAND_NAME);
  SetSerialCommandRelimiter();
  SendSerialCmdContent_Byte(iVal);
  FinishSerialCommand();
  #else
   lcd.print(iVal);
  #endif
}

bool _isSleeping = true;
void LcdSleep()
{
  _isSleeping = true;

  #ifdef AQUARIUM_CONTROL
  StartSerialCommand();
  SendSerialCmdContent_Text(LCD_BACKLIGHT_COMMAND_NAME);
  SetSerialCommandRelimiter();
  SendSerialCmdContent_Text(CommandOff);
  FinishSerialCommand();
  #else
   lcd.noBacklight();
  #endif
}

void LcdGetUp()
{
  _isSleeping = false;

  #ifdef AQUARIUM_CONTROL
  StartSerialCommand();
  SendSerialCmdContent_Text(LCD_BACKLIGHT_COMMAND_NAME);
  SetSerialCommandRelimiter();
  SendSerialCmdContent_Text(CommandOn);
  FinishSerialCommand();
  #else
   lcd.backlight();
  #endif
}

bool IsLCDSleeping()
{
  return _isSleeping;
}


void InitializeLcd()
{
#ifndef AQUARIUM_CONTROL
  lcd.begin();
#endif
  LcdGetUp();
  SetLcdBlink(false);
}

void InitFirstRow()
{
  InitLcdRow(0);
}

void InitSecondRow()
{
  InitLcdRow(1);
}

void InitBothRow()
{
  InitSecondRow();
  InitFirstRow();
  SetLcdCursor(0,0);
}


void AppendDouble_DenoteAsMultipleOf10(int value)
{
   char repr[5];
   repr[0] = GetDigitNbFromByte(value,3,1) + 48;
   repr[1] = GetDigitNbFromByte(value,3,2) + 48;
   repr[2] = '.';
   repr[3] = GetDigitNbFromByte(value,3,3) + 48;
   repr[4] = '\0';
   AppendTextInLcd(repr);
}

void PrintTime(byte hour, byte minute, byte second, bool printSecond )
{
  char repr[9];
  repr[0] = GetDigitNbFromByte(hour,2,1) + 48;
  repr[1] = GetDigitNbFromByte(hour,2,2) + 48;
  repr[2] = ':';
  repr[3] = GetDigitNbFromByte(minute,2,1) + 48;
  repr[4] = GetDigitNbFromByte(minute,2,2) + 48;
  if(printSecond)
  {
   repr[5] = ':';
   repr[6] = GetDigitNbFromByte(second,2,1) + 48;
   repr[7] = GetDigitNbFromByte(second,2,2) + 48;
   repr[8]= '\0';
  }
  else
   repr[5]= '\0';
  AppendTextInLcd(repr);
}

void PrintDate(byte date, byte month, int year)
{
  char repr[11];
  repr[0] = GetDigitNbFromByte(date,2,1) + 48;
  repr[1] = GetDigitNbFromByte(date,2,2) + 48;
  repr[2] = '/';
  repr[3] = GetDigitNbFromByte(month,2,1) + 48;
  repr[4] = GetDigitNbFromByte(month,2,2) + 48;
  repr[5] = '/';
  repr[6] = GetDigitNbFromByte(year,4,1) + 48;
  repr[7] = GetDigitNbFromByte(year,4,2) + 48;
  repr[8] = GetDigitNbFromByte(year,4,3) + 48;
  repr[9] = GetDigitNbFromByte(year,4,4) + 48;
  repr[10]= '\0';
  AppendTextInLcd(repr);
}

void AppendSpecialMessage(const byte &iVal)
{
  #ifdef AQUARIUM_CONTROL
  StartSerialCommand();
  SendSerialCmdContent_Text(LCD_APPEND_TEXT_VIA_MSGNB_COMMAND_NAME);
  SetSerialCommandRelimiter();
  SendSerialCmdContent_Byte(iVal);
  FinishSerialCommand();
  #endif
  #ifdef TVUnit_CONTROL
   lcd.print(_ArrayNames[iVal]);
  #endif
}
