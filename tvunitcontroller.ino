#define TVUnit_CONTROL

#include "Constants.h"
#include "Utils.h"
#include "Serialization.h"
#include "LCDControl.h"
#include "RTC.h"
#include "Remote.h"
#include "Wire.h"
#include "Devices.h"
#include "Devices_Init.h"
#include "Menu.h"
#include "Menu_Devices.h"
#include "Menu_Special.h"
#include "Menu_Init.h"


/*
   always on not coming                    - to be checked if fixed
   default menu socket etc                 - to be checked if fixed
   socket, times to come parallely         - not required

*/

void SaveSettings()
{
  StartReadingOrSaving();

  //started
  WriteValidityDigit();

  //each device menu
  for (byte i = 0; i < AVAILABLE_RELAY_DEVICES; i++)
  {
    WriteValidityDigit(2);
    _deviceControls[i].SaveInfo();
  }

  //finished
  WriteValidityDigit(3);

}

bool ReadSettings(bool dummyTrialRun)
{
  _dummyTrialRun = dummyTrialRun;
  StartReadingOrSaving();

  //started
  if (!CheckValidityDigit())
    return false;

  //each device menu
  for (byte i = 0; i < AVAILABLE_RELAY_DEVICES; i++)
  {
    if (!CheckValidityDigit(2))
      return false;
    _deviceControls[i].ReadInfo();
  }

  //finished
  if (!CheckValidityDigit(3))
    return false;
  return true;
}

byte _deviceRefreshTimeInSecond = 2;


void setup() {
  // put your setup code here, to run once:

  InitializeLcd();
  Serial.begin(9600);
  Wire.begin();
  InitializeRtc();
  InitializeRemote();
  InitializeRelayControlledDevices();
  InitCurrentDeviceStatus();
  InitializeSettingMenus();
  LcdSleep();

  if (ReadSettings(true))
  {
    ReadSettings(false);//read the settings only if dummy run succeeds.
  }
}

void ControlDevices()
{
  bool specialWaterChangeScenarioConsidered = false;
  for (byte i = 0; i < AVAILABLE_RELAY_DEVICES; i++)
  {
    bool value = _deviceControls[i].IsDeviceOnThisMin_BasedOnTimerAndOnStatus();
    if (!value)
      value  = _deviceControls[i].IsDeviceOnThisMin_BasedOnUptoTime();
    currentDeviceStatus[i] = value;
  }

  for (byte i = 0; i < AVAILABLE_RELAY_DEVICES; i++)
  {
    DeviceOnOff(i, currentDeviceStatus[i]);
  }

}
/*

void PrintUptoTime(byte iDeviceIndex)
{
  InitFirstRow();
  AppendSpecialMessage(_deviceControls[iDeviceIndex].GetNameIndex());
  AppendTextInLcd(" TURN-ON TILL");
  InitSecondRow();
  for (byte i = 0; i < _deviceControls[iDeviceIndex].GetTotalNbPrograms(); i++)
  {
    DeviceProgramSettings * progSetting = _deviceControls[iDeviceIndex].GetProgram(i);
    if (Circuit_ValidForUptoTime == progSetting->GetProgramOnOffControlType())
    {
      byte date, month;
      int year;
      byte end_h, end_m;
      progSetting->GetDateMonthYear(date, month, year);
      progSetting->GetEndHrAndMin(end_h, end_m);
      PrintDate(date, month, year);
      AppendTextInLcd(" ");
      PrintTime(end_h, end_m, 0, false);
      return;
    }
  }
}
*/
int _lastDeviceCheckTime=0;

void loop()
{
  delay(250);

  //check if rtc showing correct timings
  /*if(GetRtcTime().year() < 2019 || GetRtcTime().year() > 2030)
    {
     ShowVerySpecialMessage(wrongDateTimeError,1);
    }*/
  if (fabs(GetCurrentSecs() - _lastDeviceCheckTime) > _deviceRefreshTimeInSecond)
  {
    _lastDeviceCheckTime = GetCurrentSecs();
    ControlDevices();
  }


  byte btnPressed = GetRemotePressedButton();
  if (GetSecondFromLastButtonPrssedInSecond() > FLASH_SCREEN_NO_BUTTON_PRESS_TIME)
  {
      InitBothRow();
      LcdSleep();

    /*
    byte indicesOfSpeciallyOnDevices[AVAILABLE_RELAY_DEVICES];
    byte devicesFilled = 0;
    for(byte i = 0;  i< AVAILABLE_RELAY_DEVICES ; i++)
    {
        if(_deviceControls[i].IsDeviceOnThisMin_BasedOnUptoTime())
        {
           indicesOfSpeciallyOnDevices[devicesFilled] = i;
           devicesFilled++;
       }
    }
    byte eachFlashDisplayForTime = 2; //seconds
    int whichDisplay = GetSecondFromLastButtonPrssedInSecond() % ((3+devicesFilled) * eachFlashDisplayForTime); 
                                                           //total time when lcd will sleep is 3*eachFlashDisplayForTime
                                                           //total time when lcd will show uptotime is devicesFilled*eachFlashDisplayForTime, each display for eachDisplayForTime seconds
    whichDisplay = whichDisplay / eachFlashDisplayForTime;
    if(whichDisplay < devicesFilled)
    {
      LcdGetUp();
      PrintUptoTime(indicesOfSpeciallyOnDevices[whichDisplay]);
    }
    else
    {
      InitBothRow();
      LcdSleep();
    }*/
  }
  else if (btnPressed == REMOTE_NO_BUTTON_PRESSED)
  {}
  else
  {
    if (IsLCDSleeping())
    {
      DisplaySettingMenus();
    }
    else
    {
      bool understoodBtnPressed = true;
      if ( btnPressed == REMOTE_LEFT_BUTTON_PRESSED)
        RemoteLeftButtonPressed();
      else if ( btnPressed == REMOTE_RIGHT_BUTTON_PRESSED)
        RemoteRightButtonPressed();
      else if ( btnPressed == REMOTE_UP_BUTTON_PRESSED)
        RemoteUpButtonPressed();
      else if ( btnPressed == REMOTE_DOWN_BUTTON_PRESSED)
        RemoteDownButtonPressed();
      else if ( btnPressed == REMOTE_STAR_OR_BACK_BUTTON_PRESSED)
        RemoteStarOrBackButtonPressed();
      else if ( btnPressed == REMOTE_OK_OR_ENTER_BUTTON_PRESSED)
        RemoteOkOrEnterButtonPressed();
      else if ( btnPressed >= REMOTE_0_BUTTON_PRESSED && btnPressed <= REMOTE_9_BUTTON_PRESSED)
        RemoteDigitPressed(btnPressed);
      else
      {
        understoodBtnPressed = false;
      }

      if (understoodBtnPressed)
        DisplaySettingMenus();
    }
    LcdGetUp();
  }

  SaveSettings();
}
