class SpecialMenuEachItemInfo
{
     byte _namesOfUptoTimeMenu;
     byte _nbMaxDevices;
     byte * _deviceIndices;
     bool _onOff_OrUptoTime; //true - on off, false - upto time
     byte _devicesFilled;

     public:
     SpecialMenuEachItemInfo()
     {
           _namesOfUptoTimeMenu = 0;
           _nbMaxDevices = 0;
           _deviceIndices = 0;
           _onOff_OrUptoTime = true;
           _devicesFilled = 0;
     }
     void SetInfo(byte iNameIndex, byte iNbDevices, bool onOffType)
     {
          _namesOfUptoTimeMenu = iNameIndex;
          _nbMaxDevices = iNbDevices;
          _deviceIndices = new byte[_nbMaxDevices];
          _onOff_OrUptoTime = onOffType;
     }
     byte GetNameIndex(){return _namesOfUptoTimeMenu;}
     bool IsOnOffType(){return _onOff_OrUptoTime;}
     byte GetNbDevices(){return _devicesFilled;}
     byte GetDeviceAtIndex(byte iIndex){return _deviceIndices[iIndex]; }
     void AppendDeviceIndex(byte iIndex)
     {
            if(_devicesFilled ==  _nbMaxDevices)
              return;
            _deviceIndices[_devicesFilled] = iIndex;
            _devicesFilled++;
     }
};

class Menu_SpecialMenu : public Menu
{
      byte _duration_h, _duration_m;
      byte _indexOfUptoTimeMenu;
      SpecialMenuEachItemInfo * _SpecialMenuItemInfo;
      byte _nbSpecialMenus;


     bool ShouldOkBeAvailable_UptoTimeMenu()
     {
        if(GetBlinkingIndex() != 16)
           return false;
        InitBothRow();//both rows
        AppendSpecialMessage(okToSaveTextName);
        return true;
     }

  public: 

     Menu_SpecialMenu(Menu *iParent, byte iNameIndex, byte iNbSpecialMenus, SpecialMenuEachItemInfo * iSpecialMenuItemArray):Menu(iNameIndex) 
     {
        _nbSpecialMenus = iNbSpecialMenus;
        _SpecialMenuItemInfo = iSpecialMenuItemArray;
        _indexOfUptoTimeMenu = 0;
        SetHorizontalLineIndex(0);
        SetBlinkingIndex(0);
        Init(iParent,0);
        _duration_h=0;
        _duration_m=0;
        SetDefaultBlinking();
     }

     byte GeNbOfSpecialMenuItems() {return _nbSpecialMenus;}
     SpecialMenuEachItemInfo * GeSpecialMenuItemsInfo(byte iIndex) {return &_SpecialMenuItemInfo[iIndex];}
     
     void GoPrevious()
     {
        SetDefaultBlinking();
        if(_indexOfUptoTimeMenu == 0)
           _indexOfUptoTimeMenu = _nbSpecialMenus-1;
        else
           _indexOfUptoTimeMenu = _indexOfUptoTimeMenu-1;
     }
     void GoNext()
     {
        SetDefaultBlinking();
        if(_indexOfUptoTimeMenu == _nbSpecialMenus-1)
           _indexOfUptoTimeMenu = 0;
        else
           _indexOfUptoTimeMenu = _indexOfUptoTimeMenu+1;
     }

     byte GetDefaultBlinkinIndex()
     {
        if(maxTimeOfSpecialMenu > 10)
          return 10;
        else if(maxTimeOfSpecialMenu > 1)
          return 11;
        else
          return 13;
     }

      void SetDefaultBlinking()
      {
         SetBlinkingIndex(GetDefaultBlinkinIndex());
      }

      void SetTimeDateYear(IndividualDevice * device,byte date, byte month, int year, byte end_h, byte end_m)
      {
        for(byte i = 0; i< device->GetTotalNbPrograms(); i++)
        {
           
           if(Circuit_ValidForUptoTime != device->GetProgram(i)->GetProgramOnOffControlType())
              continue;

           device->GetProgram(i)->SetDateMonthYear(date,month,year);
           device->GetProgram(i)->SetEndHrAndMin(end_h,end_m);
        }
      }

void OnOk()
{
    SetDefaultBlinking();
    byte nbDevices = _SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetNbDevices();
    if (_SpecialMenuItemInfo[_indexOfUptoTimeMenu].IsOnOffType())
    {
        int currentstatus = 2;
        for (byte i = 0; i < _SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetNbDevices(); i++)
        {
            if (_deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)].GetProgram(0)->GetProgramOnOffControlType() == Circuit_ON)
            {
                AppendTextInLcd("ON");
                currentstatus = 1;
                break;
            }
            else if (_deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)].GetProgram(0)->GetProgramOnOffControlType() == Circuit_OFF)
            {
                AppendTextInLcd("OFF");
                currentstatus = 0;
                break;
            }
        }
        if (currentstatus == 1)
        {
            for (byte i = 0; i < _SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetNbDevices(); i++)
            {
                if (_deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)].GetProgram(0)->GetProgramOnOffControlType() == Circuit_ON)
                {
                    _deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)].GetProgram(0)->SetProgramOnOffControlType(Circuit_OFF);
                }
            }
        }
        else if (currentstatus == 0)
        {
            for (byte i = 0; i < _SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetNbDevices(); i++)
            {
                if (_deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)].GetProgram(0)->GetProgramOnOffControlType() == Circuit_OFF)
                {
                    _deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)].GetProgram(0)->SetProgramOnOffControlType(Circuit_ON);
                }
            }
        }
    }
    else
    {
        DateTime& currTime = GetRtcTime();
        byte date = currTime.date(), month = currTime.month();
        int year = currTime.year();
        byte  end_h = currTime.hour() + _duration_h, end_m = currTime.minute() + _duration_m;
        if (_duration_h == 0 && _duration_m == 0)
        {
            //so that devices are off immediately from upto time
            if (_duration_m > 0)
                _duration_m = _duration_m - 1;
            else if (end_h > 0)
                end_h = end_h - 1;
        }
        else
        {
            if (end_m > 59)
            {
                end_m = end_m % 60;
                end_h = end_h + 1;
            }
            if (end_h > 24)
            {
                end_h = end_h % 24;
                if (date == 31 && month == 12)//last day, change year.. assuming until date is less than 1 day
                {
                    year = year + 1;
                    date = 1;
                    month = 1;
                }
                else
                {
                    if (month == 2)
                    {
                        if (((year % 100) % 4 == 0))
                        {
                            if (date <= 28)
                                date = date+1;
                            else
                            {
                                date = 1;
                                month = 3;
                            }
                        }
                        else 
                        {
                            if (date <= 27)
                                date = date+1;
                            else
                            {
                                date = 1;
                                month = 3;
                            }
                        }
                    }
                    else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
                    {
                        if (date < 31)
                            date = date+1;
                        else
                        {
                            date = 1;
                            month = month + 1;
                        }
                    }
                    else
                    {
                        if (date < 30)
                            date = 30;
                        else
                        {
                            date = 1;
                            month = month + 1;
                        }
                    }
                }
            }
        }
        for (byte i = 0; i < _SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetNbDevices(); i++)
        {
            SetTimeDateYear(&_deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)], date, month, year, end_h, end_m);
        }
    }
}

      void DisplayMenuName()
      {
          InitFirstRow();
          if(_indexOfUptoTimeMenu > 0)
                AppendTextInLcd("<- ");
          AppendSpecialMessage(_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetNameIndex());
          if(_indexOfUptoTimeMenu < _nbSpecialMenus-1)
          {
              SetLcdCursor(14 , 0);
              AppendTextInLcd("->");
          }
          InitSecondRow();
      }



      void Display()
      {
         SetLcdBlink(false);
         if(_SpecialMenuItemInfo[_indexOfUptoTimeMenu].IsOnOffType())
         {
               DisplayMenuName();
               AppendSpecialMessage(modeTextName);
               for(byte i = 0; i< _SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetNbDevices(); i++)
              {
                 if(_deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)].GetProgram(0)->GetProgramOnOffControlType() == Circuit_ON)
                 {
                   AppendTextInLcd("ON");
                   break;
                 }
                 else if(_deviceControls[_SpecialMenuItemInfo[_indexOfUptoTimeMenu].GetDeviceAtIndex(i)].GetProgram(0)->GetProgramOnOffControlType() == Circuit_OFF)
                 {
                   AppendTextInLcd("OFF");
                   break;
                 }
              }
         }
         else
         {
            /*if(ShouldOkBeAvailable_UptoTimeMenu())
            {
               InitBothRow();//both rows
               AppendSpecialMessage(okToSaveTextName);
            }
            else*/
            {
              DisplayMenuName();
              AppendSpecialMessage(durationForUptoTIme);
              PrintTime(_duration_h,_duration_m, 0, false);
              SetLcdCursor(GetBlinkingIndex() , 1);
              SetLcdBlink(true);
           }
         }
      }


     void OnDigitPress(byte iDigit)
     {
          byte MaxHrValue = 0;
          if(maxTimeOfSpecialMenu > 0)
            MaxHrValue = maxTimeOfSpecialMenu-1;
          if(GetBlinkingIndex() ==  10 || GetBlinkingIndex() ==11)
            _duration_h = ReadDigitAndIfCorrectMoveNext(_duration_h,2/*nb of digits*/,iDigit/*pressed digit*/,0/*min limit reached*/,MaxHrValue/*max limit reached*/,10/*start index of nb*/,13/*next number location or 16 if no nb*/,false);
          else if(GetBlinkingIndex() ==  13 || GetBlinkingIndex() ==14)
            _duration_m = ReadDigitAndIfCorrectMoveNext(_duration_m,2/*nb of digits*/,iDigit/*pressed digit*/,0/*min limit reached*/,59/*max limit reached*/,13/*start index of nb*/,GetDefaultBlinkinIndex()/*next number location or 16 if no nb*/,false);
     }
};

Menu_SpecialMenu *_SpecialMenu=0;
