class RelayControlledDeviceMenu : public Menu
{
      byte data;//bit 0,1,2,3,4  for current device index, 5,6 for current program of current device  
      byte _start_h, _start_m, _end_h_Or_Duration, _end_m;

      enum RelayControlledHorEnum{E_TITLE_ONLY, E_Program_ON_OFF_CONTROL_Type, E_Program_DAY_SETTING_Type, E_TIMESET};

      /*
       * E_TIMESET:
       *    _start_h : date
       *    _start_m : month
       *    _end_h_Or_Duration : hour
       *    _end_m   : min
       *    
       */

  public:
     byte GetCurrentDeviceIndex()                           { return GetValueFromBits(&data,0,4);}
     void SetCurrentDeviceIndex(byte iValue)                { SetBitsInTarget(&data,iValue,0,4); }
     byte GetCurrentProgramIndexOfCurrentDevice()           { return GetValueFromBits(&data,5,6);}
     void SetCurrentProgramIndexOfCurrentDevice(byte iValue){ SetBitsInTarget(&data,iValue,5,6); }
  
      RelayControlledDeviceMenu(Menu *iParent,byte iNameIndex): Menu(iNameIndex)
      {
        SetCurrentDeviceIndex(0);
        SetCurrentProgramIndexOfCurrentDevice(0);
        SetHorizontalLineIndex((int)E_TITLE_ONLY);
        SetBlinkingIndex( 0);
        Init(iParent,0);
      }

      bool DoesLeftMenuExist()
      {
         if(GetCurrentDeviceIndex() == 0 && GetCurrentProgramIndexOfCurrentDevice() == 0) 
                 return false;
         return true;
      }

      bool DoesRightMenuExist()
      {
         if(GetCurrentDeviceIndex() == (AVAILABLE_RELAY_DEVICES-1)) 
         {
            if((GetCurrentProgramIndexOfCurrentDevice()+1) == _deviceControls[GetCurrentDeviceIndex()].GetTotalNbPrograms()) 
                 return false;
         }
         return true;
      }

      void GoPrevious()
      {
        if((byte)E_TITLE_ONLY == GetHorizontalLineIndex())
        {
          if(DoesLeftMenuExist())
          {
            if(GetCurrentProgramIndexOfCurrentDevice() > 0)
            {
               SetCurrentProgramIndexOfCurrentDevice(GetCurrentProgramIndexOfCurrentDevice()-1);
            }
            else
            {
               SetCurrentDeviceIndex(GetCurrentDeviceIndex()-1);
               SetCurrentProgramIndexOfCurrentDevice(_deviceControls[GetCurrentDeviceIndex()].GetTotalNbPrograms()-1);
            }
         }
        }
        else if((byte)E_Program_ON_OFF_CONTROL_Type == GetHorizontalLineIndex())
        {
             //{Circuit_OFF = 0, Circuit_ON = 1, Circuit_AUTO = 2};
            Program_ON_OFF_CONTROL_Type status = _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetProgramOnOffControlType();
            switch(status)
            {
              case Circuit_OFF:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetIfUptoTimePossible())
                 {
                    status = Circuit_ValidForUptoTime;break;
                 }
              case Circuit_ValidForUptoTime:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetIfAnyLightOnControllPossible())
                 {
                    status = Circuit_AnyLightOnControlled;break;
                 }
              case Circuit_AnyLightOnControlled:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetIfSensorControlledPossible())
                 {
                    status = Circuit_SensorControlled;break;
                 }
              case Circuit_SensorControlled:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GtIfTimeControlledPossible())
                 {
                    status = Circuit_TimeControlled;break;
                 }
              case Circuit_TimeControlled:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetIfOnPossible())
                 {
                    status = Circuit_ON;break;
                 }
              default:
                 status = Circuit_OFF;
            }
            _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->SetProgramOnOffControlType(status);
        }
        else if((byte)E_Program_DAY_SETTING_Type == GetHorizontalLineIndex())
        {
             byte nextType = MaxValueOfProgramType;
             if((byte)(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetProgramDaySettingType()) > 0)
                nextType = (byte)(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetProgramDaySettingType()) - 1;
        }
      }

      void GoNext()
      {
        byte currDeviceIndex = GetCurrentDeviceIndex();
        byte currProgramIndex = GetCurrentProgramIndexOfCurrentDevice();
        IndividualDevice * currDevice = &_deviceControls[currDeviceIndex];
        byte totalNbProgramsOnCurrDevice = currDevice->GetTotalNbPrograms();
 
        if((byte)E_TITLE_ONLY == GetHorizontalLineIndex())
        {
         if(DoesRightMenuExist())
         {
            if((currProgramIndex+1) == totalNbProgramsOnCurrDevice)
            {
               SetCurrentProgramIndexOfCurrentDevice(0);
               SetCurrentDeviceIndex(currDeviceIndex+1);
            }
            else
            {
               SetCurrentProgramIndexOfCurrentDevice(currProgramIndex+1);
            }
            currDeviceIndex = GetCurrentDeviceIndex();
            currProgramIndex = GetCurrentProgramIndexOfCurrentDevice();
         }
        }
        else if((byte)E_Program_ON_OFF_CONTROL_Type == GetHorizontalLineIndex())
        {
             //{Circuit_OFF = 0, Circuit_ON = 1, Circuit_AUTO = 2};
            Program_ON_OFF_CONTROL_Type status = _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetProgramOnOffControlType();
            switch(status)
            {
              case Circuit_OFF:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetIfOnPossible())
                 {
                    status = Circuit_ON;break;
                 }
              case Circuit_ON:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GtIfTimeControlledPossible())
                 {
                    status = Circuit_TimeControlled;break;
                 }
              case Circuit_TimeControlled:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetIfSensorControlledPossible())
                 {
                    status = Circuit_SensorControlled;break;
                 }
              case Circuit_SensorControlled:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetIfAnyLightOnControllPossible())
                 {
                    status = Circuit_AnyLightOnControlled;break;
                 }
              case Circuit_AnyLightOnControlled:
                 if(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetIfUptoTimePossible())
                 {
                    status = Circuit_ValidForUptoTime;break;
                 }
              default:
                 status = Circuit_OFF;
            }
            _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->SetProgramOnOffControlType(status);
        }
        else if((byte)E_Program_DAY_SETTING_Type == GetHorizontalLineIndex())
        {
             byte nextType = (byte)(_deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetProgramDaySettingType()) + 1;
             if(nextType >MaxValueOfProgramType )
                nextType = 0;
            _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->SetProgramDaySettingType((Program_DAY_SETTING_Type)nextType);
        }
      }

      void SetDefaultBlinkingIndex()
      {
          byte index = GetHorizontalLineIndex();
          if(index == (byte)E_Program_ON_OFF_CONTROL_Type)
             SetBlinkingIndex(6);
          if(index == (byte)E_Program_DAY_SETTING_Type)
             SetBlinkingIndex(5);
          if(index == (byte)E_TIMESET)
             SetBlinkingIndex(5);
      }

      void GoUp()
      {
          if(GetHorizontalLineIndex() > 0)
             SetHorizontalLineIndex(GetHorizontalLineIndex()-1);
          SetDefaultBlinkingIndex();
      }
      
      void GoDown()
      {
          if(GetHorizontalLineIndex() == (byte)E_TIMESET)
             return;

          if(GetHorizontalLineIndex() == (byte)E_Program_ON_OFF_CONTROL_Type && _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetProgramOnOffControlType() != Circuit_TimeControlled)
             return;

          SetHorizontalLineIndex(GetHorizontalLineIndex()+1);
          if(GetHorizontalLineIndex() ==(byte)E_TIMESET)
          {
              _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetStartHrAndMin(_start_h,_start_m);
              _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->GetEndHrAndMin(_end_h_Or_Duration,_end_m);
          }
          SetDefaultBlinkingIndex();
      }

      void OnOk()
      {
          if(GetHorizontalLineIndex() == (byte)E_TITLE_ONLY)
            GoDown();
          else if(GetHorizontalLineIndex() == (byte)E_TIMESET && GetBlinkingIndex()==16)
          {
             _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->SetStartHrAndMin(_start_h,_start_m);
             _deviceControls[GetCurrentDeviceIndex()].GetProgram(GetCurrentProgramIndexOfCurrentDevice())->SetEndHrAndMin(_end_h_Or_Duration,_end_m);
          }
          SetDefaultBlinkingIndex();
      }


      void Display()
      {
         byte nbProgram = _deviceControls[GetCurrentDeviceIndex()].GetTotalNbPrograms();
         byte currentProgramIndex = GetCurrentProgramIndexOfCurrentDevice();
         InitFirstRow();//both rows
         if(DoesLeftMenuExist() && GetHorizontalLineIndex() == (byte)E_TITLE_ONLY)
           AppendTextInLcd("<- ");
         AppendSpecialMessage(_deviceControls[GetCurrentDeviceIndex()].GetNameIndex());
         if(nbProgram > 1)
         {
           AppendTextInLcd("-P");
           AppendByte(currentProgramIndex+1);
         }
         if(DoesRightMenuExist()  && GetHorizontalLineIndex() == (byte)E_TITLE_ONLY)
         {
           SetLcdCursor(14 , 0);
           AppendTextInLcd("->");
         }
         if(GetHorizontalLineIndex() > (byte)E_TITLE_ONLY)
            AppendSpecialMessage(settingTextName);
         InitSecondRow();
         if(GetHorizontalLineIndex() == (byte)E_TITLE_ONLY)
            AppendSpecialMessage(backOkTextName);
         else if(GetHorizontalLineIndex() == (byte)E_Program_ON_OFF_CONTROL_Type)
         {
            AppendSpecialMessage(modeTextName);
            byte status = _deviceControls[GetCurrentDeviceIndex()].GetProgram(currentProgramIndex)->GetProgramOnOffControlType();
            if(status == Circuit_OFF)
                AppendTextInLcd("Off");
            else if(status == Circuit_ON)
                AppendTextInLcd("On");
            else if(status == Circuit_TimeControlled)
               AppendSpecialMessage(timerBasedTextName);
            else if(status == Circuit_ValidForUptoTime)
               AppendSpecialMessage(uptoTimeBasedTextName);
         }
         else if(GetHorizontalLineIndex() == (byte)E_Program_DAY_SETTING_Type)
         {
             AppendSpecialMessage(dayTextName);
             Program_DAY_SETTING_Type type = _deviceControls[GetCurrentDeviceIndex()].GetProgram(currentProgramIndex)->GetProgramDaySettingType();
             if(type == MTWTFSS)
             {
                AppendSpecialMessage(dailyTextName);
              }
              else if(type == MWF)
              {
                AppendSpecialMessage(MWFTextName);
              }
              else if(type == TuThSa)
              {
                AppendSpecialMessage(TuThSaTextName);
              }
              else if(type == MTWTF)
              {
                AppendSpecialMessage(MTWTFTextName);
              }
              else if(type == SaSu)
              {
                AppendSpecialMessage(SaSuTextName);
              }
         }
         else if(GetHorizontalLineIndex() == (byte)E_TIMESET)
         {
           if(GetBlinkingIndex() < 16)
           {
               AppendSpecialMessage(timeTextNameNoSpaceAfterTime);
               PrintTime(_start_h,_start_m,0,false);
               AppendTextInLcd(" ");
              if(!_deviceControls[GetCurrentDeviceIndex()].GetIfTimeInDurationOrNbOfRound())
              {
                 PrintTime(_end_h_Or_Duration,_end_m,0,false);
              }
              else
              {
              if(_end_h_Or_Duration < 10)
                AppendByte(0);
               AppendByte(_end_h_Or_Duration);
               #ifdef AQUARIUM_CONTROL
               if(GetCurrentDeviceIndex() != Food_Device_Index)
                   AppendSpecialMessage(mlTextName);
                else
                   AppendSpecialMessage(turnsTextName);
                #endif
              }       
           }
           else
           {
                 AppendSpecialMessage(okToSaveTextName);
           }
         }  

         if( GetHorizontalLineIndex() > (byte)E_TITLE_ONLY)
         {
           SetLcdCursor(GetBlinkingIndex() , 1);
           SetLcdBlink(true);
         }
         else 
         {
            SetLcdBlink(false);
         }
      }

      void OnDigitPress(byte iDigit)
      {
        if( GetHorizontalLineIndex() != (byte)E_TIMESET)
          return;
        byte currentIndex = GetBlinkingIndex();
        byte *toChange = 0;
        int minReached=0,maxReached=0;
        byte byteNbDigit = 2, startIndex=0, endIndex=16;
        bool treatDouble = false;
        
        if(currentIndex == 5 || currentIndex == 6)
        {
          toChange = &_start_h; minReached = 0; maxReached = 23; startIndex = 5; endIndex = 8;
        }
        else if(currentIndex == 8 || currentIndex == 9)
        {
          toChange = &_start_m; minReached = 0; maxReached = 59; startIndex = 8; endIndex = 11;
        }
        else if(currentIndex == 11 || currentIndex == 12)
        {
          toChange = &_end_h_Or_Duration;
           startIndex = 11; 
          if(!_deviceControls[GetCurrentDeviceIndex()].GetIfTimeInDurationOrNbOfRound())
          {
             minReached = _start_h; maxReached = 23;
             endIndex = 14;
          }
          else
          {
             minReached = 1; maxReached = 99;
             endIndex = 16;
          }
        }
        else if(currentIndex == 14 || currentIndex == 15)
        {
          toChange = &_end_m;
           startIndex = 14; endIndex = 16;
           
          if(_end_h_Or_Duration > _start_h)
          {
             minReached = 1; maxReached = 59;
          }
          else if(_end_h_Or_Duration == _start_h)
          {
             minReached = _start_m+1; maxReached = 59;
          }
        }
       if(toChange)
         *toChange = ReadDigitAndIfCorrectMoveNext(*toChange,2/*nb of digits*/,iDigit/*pressed digit*/,minReached,maxReached,startIndex,endIndex,treatDouble);
     }
};
