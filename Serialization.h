
#include <EEPROM.h>


int currentAddress = 0;
bool _dummyTrialRun=false;
void StartReadingOrSaving()
{
  currentAddress = 0;
}



void UpdateValueFromCurrentAddressAndIncrementCurrentAddress(byte value)
{
  if( currentAddress < EEPROM.length())
  {
    EEPROM.update(currentAddress,value);
    currentAddress++;
  }
}


void ReadSerializedValue(byte &value)
{
  //if( currentAddress < EEPROM.length())
  {
    if(!_dummyTrialRun)
      value = EEPROM.read(currentAddress);
    else
       EEPROM.read(currentAddress);
    
    currentAddress++;
  }
}
const byte SerializationMechanismValidityDigit = 200;
const byte SerializationMechanismValidityDigit2 = 201;
const byte SerializationMechanismValidityDigit3 = 201;

void WriteValidityDigit(byte validityDigitType = 1)
{
  byte value = SerializationMechanismValidityDigit;
  if(validityDigitType == 2)
    value = SerializationMechanismValidityDigit2;
  if(validityDigitType == 3)
    value = SerializationMechanismValidityDigit3;
   
  UpdateValueFromCurrentAddressAndIncrementCurrentAddress(value);
}


bool CheckValidityDigit(byte validityDigitType = 1)
{
  byte value = SerializationMechanismValidityDigit;
  if(validityDigitType == 2)
    value = SerializationMechanismValidityDigit2;
  if(validityDigitType == 3)
    value = SerializationMechanismValidityDigit3;
   
  byte validity = 0;
  bool runStatus = _dummyTrialRun;
  _dummyTrialRun = false;
  ReadSerializedValue(validity);
  _dummyTrialRun = runStatus;
  if(validity != value)
     return false;
  return true;
}
