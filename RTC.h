#include "Sodaq_DS3231.h"

/// *************************************************************** INITIALIZATION ********************************************

void InitializeRtc()
{
   rtc.begin();
}

/// *************************************************************** RTC RELATED ********************************************

DateTime _latestTime;
DateTime& GetRtcTime()
{
  _latestTime = rtc.now();
   return _latestTime;//get the current date-time
}

void  setRtcTime(uint8_t hour, uint8_t min, uint8_t sec)
{
   DateTime& now = GetRtcTime();
   DateTime dt(now.year(),now.month(),now.date(),hour, min,sec,now.dayOfWeek());
   rtc.setDateTime(dt);
}

void  setRtcDate(uint8_t date, uint8_t mon, uint16_t year)
{
   static byte t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };  
   year -= mon < 3;  
    byte day = ( year + year / 4 - year / 100 +  year / 400 + t[mon - 1] + date) % 7;
    uint8_t dow = day;
  
   DateTime& now = GetRtcTime();
   DateTime dt(year,mon,date,now.hour(), now.minute(),now.second(),dow);
   rtc.setDateTime(dt);
}
