

//pin 6 - controls holder
//pin 7 - controls left of 3 sockets
//pin 8 - controls middle of 3 sockets
//pin 9 - controls right of 3 sockets

//outer socket control orange 9
//left socket brown 8
//middle socket red 7
//right socket blue 6

const byte Socket_TV_Pin =6;
const byte Socket_Amplifier_Pin =7;
const byte Socket_Internet_Pin =8;
const byte Socket_ExternalSocket_Pin =9;
const byte Remotel_Pin =10;

#define Socket_TV_INDEX 0  
#define Socket_Amplifier_INDEX 1
#define Socket_Internet_INDEX 2
#define Socket_External_Socket_INDEX 3
#define AVAILABLE_RELAY_DEVICES 4

const byte Socket_TV_NAME_INDEX =0;
const byte Socket_Amplifier_NAME_INDEX =Socket_TV_NAME_INDEX+1;
const byte Socket_Internet_NAME_INDEX =Socket_Amplifier_NAME_INDEX+1;
const byte Socket_Fourth_NAME_INDEX =Socket_Internet_NAME_INDEX+1;
const byte devicesTextName = Socket_Fourth_NAME_INDEX+1;
const byte dateTimeTextName = devicesTextName+1;
const byte wrongDigitTextName = dateTimeTextName+1;
const byte okToSaveTextName = wrongDigitTextName+1;
const byte dayTextName = okToSaveTextName+1;
const byte settingTextName = dayTextName+1;
const byte backOkTextName = settingTextName+1;
const byte modeTextName = backOkTextName+1;
const byte timerBasedTextName = modeTextName+1;
const byte dailyTextName = timerBasedTextName+1;
const byte MWFTextName = dailyTextName+1;
const byte TuThSaTextName = MWFTextName+1;
const byte MTWTFTextName = TuThSaTextName+1;
const byte SaSuTextName = MTWTFTextName+1;
const byte timeTextNameNoSpaceAfterTime = SaSuTextName+1;
const byte setTextName = timeTextNameNoSpaceAfterTime+1;
const byte dateTextName = setTextName+1;
const byte okToSettingTextName = dateTextName+1;
const byte uptoTimeBasedTextName = okToSettingTextName+1;
const byte uptoTimeOnlyNetTextName = uptoTimeBasedTextName+1;
const byte uptoTimeOnlyTvTextName = uptoTimeOnlyNetTextName+1;
const byte uptoTimeTvAndAmpTextName = uptoTimeOnlyTvTextName+1;
const byte uptoTimeOnlyAmpTextName = uptoTimeTvAndAmpTextName+1;
const byte uptoTimeTvAmpAndNetTextName = uptoTimeOnlyAmpTextName+1;
const byte durationForUptoTIme = uptoTimeTvAmpAndNetTextName+1;
const byte uptoTimeTextName = durationForUptoTIme+1;
const byte SocketOnOffTextName = uptoTimeTextName+1;

const byte totalMenuItems = SocketOnOffTextName+1;
const char *_ArrayNames[totalMenuItems] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
class InitVariables
{
  public:
  InitVariables()
  {
      _ArrayNames[Socket_TV_NAME_INDEX] = "Tv";
      _ArrayNames[Socket_Amplifier_NAME_INDEX] = "Amp";
      _ArrayNames[Socket_Internet_NAME_INDEX] = "Net";
      _ArrayNames[Socket_Fourth_NAME_INDEX] = "Ext Socket";
      _ArrayNames[devicesTextName] = "Socket Sett";
      _ArrayNames[dateTimeTextName] = "Date Time";//5
      _ArrayNames[wrongDigitTextName] = "  Wrong Digit.  ";
      _ArrayNames[okToSaveTextName] = "Press ok to save.";
      _ArrayNames[dayTextName] = "Day: ";
      _ArrayNames[settingTextName] = " Setting";
      _ArrayNames[backOkTextName] = "*back; Ok to set";
      _ArrayNames[modeTextName] = "Mode: ";//11
      _ArrayNames[timerBasedTextName] = "Timer based";
      _ArrayNames[dailyTextName] = "Daily";
      _ArrayNames[MWFTextName] = "MWF";
      _ArrayNames[TuThSaTextName] = "TuThSa";
      _ArrayNames[MTWTFTextName] = "Weekday";
      _ArrayNames[SaSuTextName] = "Weekend";
      _ArrayNames[timeTextNameNoSpaceAfterTime] = "Time:";//18
      _ArrayNames[setTextName] = " Set";
      _ArrayNames[dateTextName] = "Date: ";
      _ArrayNames[okToSettingTextName] = "  Ok to Setting";
      _ArrayNames[uptoTimeBasedTextName] = "upto time";//23
      _ArrayNames[uptoTimeOnlyNetTextName] = "Only Net";
      _ArrayNames[uptoTimeOnlyTvTextName] = "Only TV";
      _ArrayNames[uptoTimeTvAndAmpTextName] = "Tv & Amp";
      _ArrayNames[uptoTimeOnlyAmpTextName] = "Only Amp";
      _ArrayNames[uptoTimeTvAmpAndNetTextName] = "Tv,Net,Amp";
      _ArrayNames[durationForUptoTIme] = "Duration :";//28
      _ArrayNames[uptoTimeTextName] = "Turn-On till";
      _ArrayNames[SocketOnOffTextName] = "Ext Socket";
   }
};
InitVariables variables;



char weekDay[7][4] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
enum DayEnum{SUNDAY=0,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY};

#define RELAY_ON LOW
#define RELAY_OFF HIGH


bool currentDeviceStatus[AVAILABLE_RELAY_DEVICES];
#define FLASH_SCREEN_NO_BUTTON_PRESS_TIME 10

byte maxTimeOfSpecialMenu = 3; //not reached  
