 
//main menus: Devices, test
class Menu  
{
     Menu * m_ParentMenu;
     Menu ** m_SubMenuItem;
     byte m_MenuNameIndex;
     byte available_TotalSub_menu;//bit 0,1,2,3  for total sub menus, 4,5,6,7 for current menu
     byte data; //0,1,2,3,4 for blinking index , 5,6,7 for horizontal line

  public:
      Menu(const byte &menu_Name_Index):m_MenuNameIndex(menu_Name_Index)
      {
         m_SubMenuItem = 0;
         SetTotalInsertedMenus(0);
         SetCurrentMenuIndex(0);
      }

      Menu * GetParentMenu() { return m_ParentMenu;}
      byte GetMenuNameIndex()   { return m_MenuNameIndex;}

     byte GetHorizontalLineIndex()                          {return GetValueFromBits(&data,5,7);}
     void SetHorizontalLineIndex(byte iValue)               {SetBitsInTarget(&data,iValue,5,7);}
     byte GetBlinkingIndex()                                {return GetValueFromBits(&data,0,4);}
     void SetBlinkingIndex(byte iValue)                     {SetBitsInTarget(&data,iValue,0,4);}
      byte GetTotalInsertedMenus()            { return GetValueFromBits(&available_TotalSub_menu,0,3);}
      void SetTotalInsertedMenus(byte iValue) {SetBitsInTarget(&available_TotalSub_menu,iValue,0,3);}
      byte GetCurrentMenuIndex()              {return GetValueFromBits(&available_TotalSub_menu,4,7);}
      void SetCurrentMenuIndex(byte iValue)   {SetBitsInTarget(&available_TotalSub_menu,iValue,4,7);}

      Menu * GetCurrentSubMenu()
      {
          if(GetTotalInsertedMenus()==0) 
            return 0;
          return m_SubMenuItem[GetCurrentMenuIndex()];
      }

        
     void Init(Menu * iParentMenu, byte maxSubItems)
     {
         m_ParentMenu=iParentMenu;
         if(maxSubItems > 0)
            m_SubMenuItem = new Menu*[maxSubItems];
         SetTotalInsertedMenus(0);
         SetCurrentMenuIndex(0);
     }

      void AddItem(Menu * iSubMenu)
      {
           m_SubMenuItem[GetTotalInsertedMenus()] = iSubMenu;
           SetTotalInsertedMenus(GetTotalInsertedMenus()+1);
      }


      void DisplayWrongDigitPressedMsg()
      {
           InitBothRow();//init both rows
           AppendSpecialMessage(wrongDigitTextName);
           delay(1000);
           Display();
      }
      
     virtual void GoPrevious(){};
     virtual void GoNext(){};
     virtual void GoUp(){};
     virtual void GoDown(){};
     virtual void OnDigitPress(byte iDigit){};
     virtual void OnOk(){};
     virtual void Display(){};

     bool DisplayOkToSaveMsg_IfAvailable()
     {
        if(GetBlinkingIndex() != 16)
           return false;
        if(GetHorizontalLineIndex() == 0)
           return false;
        InitBothRow();//both rows
        AppendSpecialMessage(okToSaveTextName);
        return true;
     }

     int ReadDigitAndIfCorrectMoveNext(int iCurrentValue,byte iNbDigits, byte iDigit, int iMinValReached,int iMaxValReached, byte startBlinkingIndex,  byte nextValidDigitIndexWhenDigitFinished, bool treatAsDouble_IntSentMultipliedBy10)
     {
        // 1345    1 is digit#1,  5 is digit#4
       byte digitNb = GetBlinkingIndex()-startBlinkingIndex + 1;
       if(treatAsDouble_IntSentMultipliedBy10 && digitNb == 4)
          digitNb = 3;
       int tmp = iCurrentValue;

       byte newDigit[4] = {GetDigitNbFromByte(iCurrentValue,4,1),GetDigitNbFromByte(iCurrentValue,4,2),GetDigitNbFromByte(iCurrentValue,4,3),GetDigitNbFromByte(iCurrentValue,4,4)};
       byte currentIndexOfDigit = 4-iNbDigits+digitNb-1; // if iNbDigits = 2 &&& digitNb = 1 => currentIndexOfDigit = 2
                                                         // if iNbDigits = 2 &&& digitNb = 2 => currentIndexOfDigit = 3
       newDigit[4-iNbDigits+digitNb-1] = iDigit;
       tmp = GetIntFromDigit(newDigit[0],newDigit[1],newDigit[2],newDigit[3]);

       if(tmp < iMinValReached ||  tmp > iMaxValReached)
       {
          for(byte k = currentIndexOfDigit+1; k<=3; k++)
             newDigit[k] = 0;
          tmp = GetIntFromDigit(newDigit[0],newDigit[1],newDigit[2],newDigit[3]);
          if(tmp < iMinValReached)
             tmp = iMinValReached;
          if(tmp > iMaxValReached)
             tmp = iMaxValReached;
       }  
       if(digitNb  == iNbDigits)
         SetBlinkingIndex(nextValidDigitIndexWhenDigitFinished);
       else if(treatAsDouble_IntSentMultipliedBy10 && digitNb==2)
         SetBlinkingIndex(startBlinkingIndex+digitNb+1);
       else
         SetBlinkingIndex(startBlinkingIndex+digitNb);
       return tmp;
     }

      void PrintSensorValue(byte iTxtMsgIndex, int * iValue)
      {
           if(GetBlinkingIndex() < 16)
           {
               AppendSpecialMessage(iTxtMsgIndex);
               AppendDouble_DenoteAsMultipleOf10(*iValue);
           }
           else
           {
               AppendSpecialMessage(okToSaveTextName);
           }
      }

};

Menu *_currentlyActiveMenu=0;


void DisplaySettingMenus()
{
    SetLcdBlink(false);
    _currentlyActiveMenu->Display();
}

void RemoteLeftButtonPressed()
{
     _currentlyActiveMenu->GoPrevious();
}

void RemoteRightButtonPressed()
{
     _currentlyActiveMenu->GoNext();
}

void RemoteStarOrBackButtonPressed()
{
     if(_currentlyActiveMenu->GetHorizontalLineIndex() > 0)
      _currentlyActiveMenu->SetHorizontalLineIndex(0);
     else if(_currentlyActiveMenu->GetParentMenu())
     {
       _currentlyActiveMenu  = _currentlyActiveMenu->GetParentMenu();
     }
     #ifdef AQUARIUM_CONTROL
     else
       ForceScreenSaver();
     #endif
}

void RemoteOkOrEnterButtonPressed()
{
     _currentlyActiveMenu->OnOk();
}

void RemoteDownButtonPressed()
{
     _currentlyActiveMenu->GoDown();
}

void RemoteUpButtonPressed()
{
     _currentlyActiveMenu->GoUp();
}

void RemoteDigitPressed(byte iDigit)
{
     _currentlyActiveMenu->OnDigitPress(iDigit);
}

class MainMenu : public Menu
{
  private:
 
  public:
      MainMenu(byte maxSubMenus, const byte &iNameIndex):Menu(iNameIndex)
      {
        Init(0,maxSubMenus);
      }

     void GoPrevious()
     {
        if(GetCurrentMenuIndex() > 0)
           SetCurrentMenuIndex(GetCurrentMenuIndex()-1);
     }

     void GoNext()
     {
        if(GetCurrentMenuIndex() < GetTotalInsertedMenus()-1)
           SetCurrentMenuIndex(GetCurrentMenuIndex()+1);
     }

      void OnOk()
      {
        _currentlyActiveMenu  = _currentlyActiveMenu->GetCurrentSubMenu();
      }
     
      void Display()
      {
         InitFirstRow();
         if(GetCurrentMenuIndex() > 0)
           AppendTextInLcd("<- ");
         if(GetCurrentSubMenu())
            AppendSpecialMessage(GetCurrentSubMenu()->GetMenuNameIndex());
         if(GetCurrentMenuIndex() < GetTotalInsertedMenus()-1)
         {
           SetLcdCursor(14 , 0);
           AppendTextInLcd("->");
         }

         InitSecondRow();
         if(GetCurrentSubMenu())
             AppendSpecialMessage(okToSettingTextName);
      
      }
};



class DateAndTimeMenu : public Menu
{
      //GetHorizontalLineIndex 0 for title
      //GetHorizontalLineIndex 1 for Time
      //GetHorizontalLineIndex 2 for Date
      
      byte _val1, _val2;
      int _year;
  public: 

     DateAndTimeMenu(Menu *iParent, byte iNameIndex):Menu(iNameIndex)
     {
        SetHorizontalLineIndex(0);
        SetBlinkingIndex(0);
        Init(iParent,0);
     }
     void GoPrevious(){}
     void GoNext(){}

      void SetDefaultBlinking()
      {
         if(GetHorizontalLineIndex() == 1)
            SetBlinkingIndex(6);
         else if(GetHorizontalLineIndex() == 2)
            SetBlinkingIndex(6);
      }

      void RefreshVariables()
      {
        DateTime& now = GetRtcTime();
        if(GetHorizontalLineIndex() == 1)
        {
           _val1 = now.hour();
           _val2 = now.minute();
        }
        else if(GetHorizontalLineIndex() == 2)
        {
           _val1 = now.date();
           _val2 = now.month();
           _year = now.year();
        }
      }

      void GoUp()
      {
        if(GetHorizontalLineIndex() > 0)
             SetHorizontalLineIndex(GetHorizontalLineIndex()-1);

        RefreshVariables();
        SetDefaultBlinking();
      }
      
      void GoDown()
      {
        if(GetHorizontalLineIndex() < 2)
           SetHorizontalLineIndex(GetHorizontalLineIndex()+1);

        RefreshVariables();
        SetDefaultBlinking();
      }

      void OnOk()
      {
          if(GetHorizontalLineIndex() == 0)
          {
            GoDown();
          }
          else if(GetBlinkingIndex() == 16 && (GetHorizontalLineIndex() == 1) )
          {
            setRtcTime(_val1,_val2,0);
          }
          else if(GetBlinkingIndex() == 16 && (GetHorizontalLineIndex() == 2) )
          {
               setRtcDate(_val1,_val2,_year);
          }
          SetDefaultBlinking();
      }

      void Display()
      {
         SetLcdBlink(false);
         if(DisplayOkToSaveMsg_IfAvailable())
           return;
         InitFirstRow();
         AppendSpecialMessage(GetMenuNameIndex());
         if(GetHorizontalLineIndex() > 0)
            AppendSpecialMessage(setTextName);

         InitSecondRow();
         if(GetHorizontalLineIndex() == 0)
         {
            AppendSpecialMessage(backOkTextName);
         }
         else if(GetHorizontalLineIndex() ==1)
         {
           AppendSpecialMessage(timeTextNameNoSpaceAfterTime);
           AppendTextInLcd(" ");
           PrintTime(_val1,_val2, 0, false);
         }  
         else if(GetHorizontalLineIndex() ==2)
         {
           AppendSpecialMessage(dateTextName);
           PrintDate(_val1,_val2,_year);
         }  
         
         if( GetHorizontalLineIndex() > 0)
         {
           SetLcdCursor(GetBlinkingIndex() , 1);
           SetLcdBlink(true);
         }
      }


     void OnDigitPress(byte iDigit)
     {
        if( GetHorizontalLineIndex() == 1)//Time: 12:34
        {
          if(GetBlinkingIndex() ==  6 || GetBlinkingIndex() ==7)
            _val1 = ReadDigitAndIfCorrectMoveNext(_val1,2/*nb of digits*/,iDigit/*pressed digit*/,0/*min limit reached*/,23/*max limit reached*/,6/*start index of nb*/,9/*next number location or 16 if no nb*/,false);
          else if(GetBlinkingIndex() ==  9 || GetBlinkingIndex() ==10)
          {
            if(_val1 > 23)
              _val1 = 23;
            if(_val2 > 59)
              _val2 = 59;
            _val2 = ReadDigitAndIfCorrectMoveNext(_val2,2/*nb of digits*/,iDigit/*pressed digit*/,0/*min limit reached*/,59/*max limit reached*/,9/*start index of nb*/,16/*next number location or 16 if no nb*/,false);
          }
        }
        else if( GetHorizontalLineIndex() == 2)//Date: 12/01/2002
        {
          if(_val1 > 31)
            _val1 = 31;
          if(_val2 > 12)
            _val2 = 12;
          if(_year > 2039)
            _year = 2019;

          byte currentBlinkingIndex = GetBlinkingIndex();
          if(currentBlinkingIndex ==  6 || currentBlinkingIndex ==7)
            _val1 = ReadDigitAndIfCorrectMoveNext(_val1,2/*nb of digits*/,iDigit/*pressed digit*/,1/*min limit reached*/,31/*max limit reached*/,6/*start index of nb*/,9/*next number location or 16 if no nb*/,false);
          else if(currentBlinkingIndex ==  9 || currentBlinkingIndex ==10)
            _val2 = ReadDigitAndIfCorrectMoveNext(_val2,2/*nb of digits*/,iDigit/*pressed digit*/,1/*min limit reached*/,12/*max limit reached*/,9/*start index of nb*/,12/*next number location or 16 if no nb*/,false);
          else if(currentBlinkingIndex >=  12 || currentBlinkingIndex <= 15)
            _year = ReadDigitAndIfCorrectMoveNext(_year,4/*nb of digits*/,iDigit/*pressed digit*/,2019/*min limit reached*/,2030/*max limit reached*/,12/*start index of nb*/,16/*next number location or 16 if no nb*/,false);
        }
     }
};
